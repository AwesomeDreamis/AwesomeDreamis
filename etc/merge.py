def merge(array):
    def merger(arr):
        for i in arr:
            if isinstance(i, int):
                nonlocal ar
                ar.append(i)
            else:
                merger(i)
    ar = []
    merger(array)
    return ar


A = (5, 2, 4, 6, 1, 3, 2, 6)

print(merge([A, 1, len(A)]))

