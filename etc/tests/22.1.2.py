import os


def print_dirs(project):
    print('\nСодержимое директории', project)
    for i_elem in os.listdir(project):
        path = os.path.join(project, i_elem)
        print(path)


path_to_project = os.path.abspath(os.path.join('../../..', 'python_basic'))
print_dirs(path_to_project)
