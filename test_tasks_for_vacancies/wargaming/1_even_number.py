# Написать алгоритм (функцию) определения четности целого числа, который будет аналогичен нижеприведенному по функциональности, но отличен по своей сути.
# Объяснить плюсы и минусы обеих реализаций.

import timeit

# Wargaming example:
code_to_test = """
def isEven(value):
    return value % 2 == 0


print(isEven(4))
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)

#####################################################################
code_to_test = """
def isEven_analog(value):
    return value & 1 == 0


print(isEven_analog(6))
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)


# во втором алгоритме оператор & работатет с кодом на битовом уровне, так все чётные числа будут заканчиваться на 0
# также этот код работает быстрее примерно в 2 раза при проверке модулем timeit 
