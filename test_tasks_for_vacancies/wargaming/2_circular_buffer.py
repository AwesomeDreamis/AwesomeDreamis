class CircularBuffer:

    def __init__(self, max_size=10):
        self.max_size = max_size
        self.buffer = [None] * max_size
        self.head = 0
        self.tail = -1
        self.size = 0

    def __str__(self):
        return str([item for item in self.buffer])

    def size(self):
        if self.tail >= self.head:
            return self.tail - self.head
        return self.max_size - self.head - self.tail

    def enqueue(self, item):
        if self.is_full():
            raise OverflowError(
                "CircularBuffer is full, unable to enqueue item")
        self.tail = (self.tail + 1) % self.max_size
        self.buffer[self.tail] = item
        self.size = self.size + 1

    def dequeue(self):
        if self.is_empty():
            raise IndexError("CircularBuffer is empty, unable to dequeue")
        item = self.buffer[self.head]
        self.buffer[self.head] = None
        self.head = (self.head + 1) % self.max_size
        self.size = self.size - 1
        return item

    def is_empty(self):
        return self.size == 0

    def is_full(self):
        return self.size == self.max_size

    def front(self):
        return self.buffer[self.head]


example = CircularBuffer(5)
print(f"Empty: {example.is_empty()}")
print(f"Full: {example.is_full()}")
print(example)

example.enqueue("one")
example.enqueue("two")
example.enqueue("three")
example.enqueue("four")
example.enqueue("five")
print(example)

example.dequeue()
print(example)

example.enqueue("six")
print(example)

print(f"Full: {example.is_full()}")

print(example.front())

