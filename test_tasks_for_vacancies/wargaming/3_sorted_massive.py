# Написать функцию, которая быстрее всего (по процессорным тикам) отсортирует данный ей массив чисел.
# Массив может быть любого размера со случайным порядком чисел (в том числе и отсортированным).
#
# Объяснить почему вы считаете, что функция соответствует заданным критериям.

import timeit


# сортировка выбором 1
print('сортировка выбором 1: ')
code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

def find_smallest(arr):
    smallest = arr[0]
    smallest_index = 0

    for i in range(1, len(arr)):
        if arr[i] < smallest:
            smallest = arr[i]
            smallest_index = i

    return smallest_index


# сортировка выбором на основе предыдущей функции
def selection_sort(arr):
    new_arr = []

    for i in range(len(arr)):
        smallest = find_smallest(arr)
        new_arr.append(arr.pop(smallest))

    return new_arr


selection_sort(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)


# сортировка выбором 2
print('сортировка выбором 2: ')
code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

def s_sort(arr):
    for i in range(len(arr)):
        minimum = i

        for j in range(i + 1, len(arr)):
            # Выбор наименьшего значения
            if arr[j] < arr[minimum]:
                minimum = j

        # Помещаем это перед отсортированным концом массива
        arr[minimum], arr[i] = arr[i], arr[minimum]

    return arr


s_sort(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)


# быстрая сортировка
print('быстрая сортировка: ')
code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

def quicksort(array):
    if len(array) < 2:
        return array
    else:
        pivot = array[0]  # опорный элемент
        less = [i for i in array[1:] if i <= pivot]
        greater = [i for i in array[1:] if i > pivot]

        return quicksort(less) + [pivot] + quicksort(greater)


quicksort(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)


# Сортировка слиянием
print('сортировка слиянием: ')
code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

def merge(left, right, merged):
    left_cursor, right_cursor = 0, 0
    while left_cursor < len(left) and right_cursor < len(right):

        # Сортируем каждый и помещаем в результат
        if left[left_cursor] <= right[right_cursor]:
            merged[left_cursor + right_cursor] = left[left_cursor]
            left_cursor += 1
        else:
            merged[left_cursor + right_cursor] = right[right_cursor]
            right_cursor += 1

    for left_cursor in range(left_cursor, len(left)):
        merged[left_cursor + right_cursor] = left[left_cursor]

    for right_cursor in range(right_cursor, len(right)):
        merged[left_cursor + right_cursor] = right[right_cursor]

    return merged


def merge_sort(arr):
    # Последнее разделение массива
    if len(arr) <= 1:
        return arr
    mid = len(arr) // 2
    # Выполняем merge_sort рекурсивно с двух сторон
    left, right = merge_sort(arr[:mid]), merge_sort(arr[mid:])

    # Объединяем стороны вместе
    return merge(left, right, arr.copy())


merge_sort(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)


# сортировка пузырьком (bubble sort)
print('сортировка пузырьком: ')
code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

def bubble_sort(arr):
    def swap(i, j):
        arr[i], arr[j] = arr[j], arr[i]

    n = len(arr)
    swapped = True
    x = -1

    while swapped:
        swapped = False
        x = x + 1

        for i in range(1, n - x):
            if arr[i - 1] > arr[i]:
                swap(i - 1, i)
                swapped = True


bubble_sort(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)


# сортировка вставками (insertion sort)
print('сортировка вставками: ')
code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

def insertion_sort(arr):
    for i in range(len(arr)):
        cursor = arr[i]
        pos = i

        while pos > 0 and arr[pos - 1] > cursor:
            # Меняем местами число, продвигая по списку
            arr[pos] = arr[pos - 1]
            pos = pos - 1
        # Остановимся и сделаем последний обмен
        arr[pos] = cursor

    return arr


insertion_sort(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)





code_to_test = """
import random


massive = [i for i in range(1000)]
random.shuffle(massive)

sorted(massive)
"""

elapsed_time = timeit.timeit(code_to_test, number=1)
print(elapsed_time)



# Если в массиве случайно распределены элементы, то оптимальным вариантом будет быстрая сортировка
# Но самая быстрая сортировка это уже встроенная в Python "sorted"
