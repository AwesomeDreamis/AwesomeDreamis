import os
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

PATH = os.path.dirname(os.path.realpath(__file__)) + ""

to_int24 = lambda value: 24 if value == "all" else int(value)

tmp_cpu = pd.read_csv(
    PATH,
    delimiter=";",
    header=2,
    nrows=4875,
    converters={"CPU": to_int24},
    parse_dates=[0,],
    infer_datetime_format="%H:%M:%S",
    dtype={"%idle": np.float64},
    decimal=","
)
tmp_cpu[["%load"]] = 100 - tmp_cpu[["%idle"]]
cpu = tmp_cpu[["Time", "CPU", "%load"]]

###

fig, axes = plt.subplots(figsize=(16.5, 10))
count = 3
top = {}

for i in range(24):
    one_cpu = cpu.copy().loc[cpu["CPU"] == i]
    top[one_cpu["%load"].mean()] = (i, one_cpu)

    if len(top) > count:
        del top[min(top)]
# for _, one_cpu in top_values():

