# import sys
# print(sys.argv)

num_list = []

while True:
    try:
        num = int(input("Введите число: "))
        num_list.append(num)
    except:
        break

# Числа
for i in num_list:
    print(i , end=' ')
print()

# Количество
print(len(num_list))

# Сумма
print(sum(num_list))

# Наименьшее, наибольшее, среднее
print(min(num_list), max(num_list), sum(num_list) / len(num_list))
