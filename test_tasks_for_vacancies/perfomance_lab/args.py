import argparse


parser = argparse.ArgumentParser()
parser.add_argument("a", default=1, help="first argument")
parser.add_argument("b", default=2, help="second argument")
args = parser.parse_args()

# аргументы a и b вводятся через командную строку
# print(args.a + args.b)
