import math
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("circle_arguments", type=str, help="first argument")
parser.add_argument("points_arguments", type=str, help="second argument")
args = parser.parse_args()


with open(f'{args.circle_arguments}', 'r', encoding='utf-8') as file:  # вместо circle_arguments у меня был file1.txt
    center = list(file.readline().rstrip().split())
    circle = {'x': float(center[0]),
              'y': float(center[1]),
              'radius': float(file.readline().rstrip())}

points = {}
with open(f'{args.points_arguments}', 'r', encoding='utf-8') as file:  # вместо points_arguments у меня был file2.txt
    for num, string in enumerate(file):
        points[num] = {'x': float(string[0]), 'y': float(string[2])}

for i in points:
    hypotenuse = math.sqrt((points[i]['x'] - circle['x']) ** 2 + (points[i]['y'] - circle['y']) ** 2)

    if hypotenuse == circle['radius']:
        print("0\n")  # 0 - точка лежит на окружности
    elif hypotenuse < circle['radius']:
        print("1\n")  # 1 - точка внутри
    else:
        print("2\n")  # 2 - точка снаружи
