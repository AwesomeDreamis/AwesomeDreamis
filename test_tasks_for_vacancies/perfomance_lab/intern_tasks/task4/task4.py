import argparse


parser = argparse.ArgumentParser()
parser.add_argument("file_arguments", type=str, help="first argument")
args = parser.parse_args()


nums = []

with open(f'{args.file_arguments}', 'r', encoding='utf-8') as file:  # вместо file_arguments у меня был file.txt
    for num in file:
        nums.append(int(num.rstrip()))

nums = sorted(nums)
average = sum(nums) // len(nums)
step_count = 0

for i in nums:
    while i != average:
        if i < average:
            i += 1
            step_count += 1
        if i > average:
            i -= 1
            step_count += 1

print(f"{step_count}\n")
