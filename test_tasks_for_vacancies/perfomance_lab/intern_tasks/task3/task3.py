import json
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("tests_arg", type=str, help="first argument")
parser.add_argument("values_arg", type=str, help="second argument")
args = parser.parse_args()


def find_id(value, data):
    for test in data:
        if value['id'] == test['id']:
            test['value'] = value['value']
        if 'values' in test.keys():
            find_id(value, test['values'])


with open(f'{args.tests_arg}', 'r', encoding='utf-8') as file:
    tests_data = json.load(file)

with open(f'{args.values_arg}', 'r', encoding='utf-8') as file:
    values_data = json.load(file)


for id_data in values_data['values']:
    find_id(id_data, tests_data['tests'])


with open('report.json', 'w', encoding='utf-8') as file:
    json.dump(tests_data, file)

