import argparse


parser = argparse.ArgumentParser()
parser.add_argument("n", type=int, help="first argument")
parser.add_argument("m", type=int, help="second argument")
args = parser.parse_args()

massive = [i for i in range(1, args.n + 1)]

result = [1]

while massive[args.m - 1] != 1:
    massive = massive[args.m - 1:: 1] + massive[0: args.m - 1:]
    result.append(massive[0])

result = int("".join(map(str, result)))
print(f"{result}\n")

