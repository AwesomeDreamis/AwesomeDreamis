from django.urls import path
from app_mailings_api.views import \
    MailingAPI, MailingDetailAPI, \
    ClientAPI, ClientDetailAPI, \
    MessageAPI, MessageDetailAPI


urlpatterns = [
    path('mailings/', MailingAPI.as_view(), name='mailing_api_list'),
    path('mailings/<int:pk>/', MailingDetailAPI.as_view(), name='mailing_api_detail'),

    path('clients/', ClientAPI.as_view(), name='clients_api_list'),
    path('clients/<int:pk>/', ClientDetailAPI.as_view(), name='clients_api_detail'),

    path('messages/', MessageAPI.as_view(), name='messages_api_list'),
    path('messages/<int:pk>/', MessageDetailAPI.as_view(), name='messages_api_detail'),
]
