from django.db.models import QuerySet
from app_mailings_api.models import Mailing, Client, Message
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.generics import GenericAPIView
from app_mailings_api.features import CustomPagination
from app_mailings_api.serializers import MailingSerializer, ClientSerializer, MessageSerializer
import logging


logger = logging.getLogger(__name__)


class MailingAPI(ListModelMixin, CreateModelMixin, GenericAPIView):
    """API представление для получения списка рассылок"""

    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    pagination_class = CustomPagination
    filter_backends = [DjangoFilterBackend]

    def get_queryset(self) -> QuerySet:
        """
        Получение множества рассылок
        :return: Множество авторов
        :rtype: QuerySet
        """
        queryset = Mailing.objects.all()
        search_request = self.request.query_params.get('code')
        if search_request:
            queryset = queryset.filter(mobile_operator_code__icontains=search_request)
        return queryset

    def get(self, request):
        """Получение данных с сервера"""
        return self.list(request)

    def post(self, request, format=None):
        """Передача данных на сервер. Создание объекта"""

        return self.create(request)


class MailingDetailAPI(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, GenericAPIView):
    """API представление для получения детальной информации о рассылке,
    а также для его редактирвоания и удаления"""

    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def get(self, request, *args, **kwargs):
        """Получение объекта"""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Изменение объекта"""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Удаление объекта"""
        return self.destroy(request, *args, **kwargs)


#################################################################################


class ClientAPI(ListModelMixin, CreateModelMixin, GenericAPIView):
    """API представление для получения списка клиентов"""

    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    pagination_class = CustomPagination
    filter_backends = [DjangoFilterBackend]

    def get_queryset(self) -> QuerySet:
        """
        Получение множества клиентов
        :return: Множество авторов
        :rtype: QuerySet
        """

        queryset = Client.objects.all()
        search_code = self.request.query_params.get('code')
        search_tag = self.request.query_params.get('code')
        if search_code:
            queryset = queryset.filter(mobile_operator_code__icontains=search_code)
        if search_code:
            queryset = queryset.filter(tag__icontains=search_tag)
        return queryset

    def get(self, request):
        """Получение данных с сервера"""
        return self.list(request)

    def post(self, request, format=None):
        """Передача данных на сервер. Создание объекта"""
        return self.create(request)


class ClientDetailAPI(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, GenericAPIView):
    """API представление для получения детальной информации о клиенте,
    а также для его редактирвоания и удаления"""

    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def get(self, request, *args, **kwargs):
        """Получение объекта"""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Изменение объекта"""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Удаление объекта"""
        return self.destroy(request, *args, **kwargs)


#################################################################################


class MessageAPI(ListModelMixin, CreateModelMixin, GenericAPIView):
    """API представление для получения списка сообщений"""

    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    pagination_class = CustomPagination

    def get(self, request):
        """Получение данных с сервера"""
        return self.list(request)

    def post(self, request, format=None):
        """Передача данных на сервер. Создание объекта"""
        return self.create(request)


class MessageDetailAPI(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, GenericAPIView):
    """API представление для получения детальной информации о сообщении,
    а также для его редактирвоания и удаления"""

    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get(self, request, *args, **kwargs):
        """Получение объекта"""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Изменение объекта"""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Удаление объекта"""
        return self.destroy(request, *args, **kwargs)
