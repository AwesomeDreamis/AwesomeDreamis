from django.contrib import admin
from app_mailings_api.models import Mailing, Client, Message


class MessagesInLine(admin.TabularInline):
    model = Message


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ['id', 'start_datetime', 'end_datetime', 'mobile_operator_code', 'tag']
    inlines = [MessagesInLine, ]


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'phone_number', 'mobile_operator_code', 'tag', 'time_zone']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['id', 'created_at', 'status', 'mailing', 'client']
