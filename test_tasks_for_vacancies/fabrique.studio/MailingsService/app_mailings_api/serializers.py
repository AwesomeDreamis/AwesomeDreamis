from app_mailings_api.models import Mailing, Client, Message
from rest_framework import serializers


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ['id', 'start_datetime', 'end_datetime', 'mobile_operator_code', 'tag', 'text']


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'mobile_operator_code', 'tag', 'time_zone']


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'created_at', 'status', 'mailing', 'client']
