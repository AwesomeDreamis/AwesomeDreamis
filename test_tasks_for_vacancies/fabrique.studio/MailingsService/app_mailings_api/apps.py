from django.apps import AppConfig


class AppMailingsApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_mailings_api'
