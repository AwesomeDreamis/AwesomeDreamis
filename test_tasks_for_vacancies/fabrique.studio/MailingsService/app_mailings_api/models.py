from django.db import models


class Client(models.Model):
    TIME_ZONES = [
        ('UTC+2', 'UTC+2, Калининград'),
        ('UTC+3', 'UTC+3, Москва'),
        ('UTC+4', 'UTC+4, Самара'),
        ('UTC+5', 'UTC+5, Екатеринбург'),
        ('UTC+6', 'UTC+6, Омск'),
        ('UTC+7', 'UTC+7, Красноярск'),
        ('UTC+8', 'UTC+8, Иркутск'),
        ('UTC+9', 'UTC+9, Якутск'),
        ('UTC+10', 'UTC+10, Владивосток'),
        ('UTC+11', 'UTC+11, Магадан'),
        ('UTC+12', 'UTC+12, Камчатка'),
    ]

    def get_mobile_operator_code(self):
        return self.mobile_operator_code[1:4]

    phone_number = models.CharField(max_length=11, unique=True, verbose_name='номер телефона')
    mobile_operator_code = models.CharField(max_length=3, verbose_name='код мобильного оператора')
    tag = models.CharField(max_length=15, blank=True, verbose_name='тег')
    time_zone = models.CharField(max_length=15, choices=TIME_ZONES, default=None, verbose_name='часовой пояс')

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return str(self.id)


class Mailing(models.Model):
    try:
        CODES = set([(i.mobile_operator_code, i.mobile_operator_code) for i in Client.objects.all()])
    except:
        CODES = None
    try:
        TAGS= set([(i.tag, i.tag) for i in Client.objects.all()])
    except:
        TAGS = None

    start_datetime = models.DateTimeField(default=None, verbose_name='start_datetime')
    end_datetime = models.DateTimeField(default=None, verbose_name='end_datetime')
    text = models.CharField(max_length=1500, default='', verbose_name='text')
    mobile_operator_code = models.CharField(max_length=3, choices=CODES, verbose_name='mobile_operator_codes')
    tag = models.CharField(max_length=15, choices=TAGS, verbose_name='tags')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return str(self.id)

    def get_messages_count(self):
        return Message.objects.filter().count()


class Message(models.Model):
    STATUS_CHOICES = [
        ('SENT', "Отправлено"),
        ('FAILED', "Ошибка"),
    ]

    created_at = models.DateTimeField(default=None, verbose_name='created_at')
    status = models.CharField(max_length=15, choices=STATUS_CHOICES, verbose_name='status')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='mailing')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='client')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return str(self.id)
