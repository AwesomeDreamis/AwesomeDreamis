import datetime
from datetime import datetime
from django.core.exceptions import PermissionDenied
from django.db.models import QuerySet
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views import View
from app_mailings_api.models import Mailing, Client, Message
from app_mailings.forms import MailingForm
from django.views.generic import ListView, DeleteView, DetailView, CreateView, UpdateView
import logging


logger = logging.getLogger(__name__)


class MainPage(ListView):
    model = Mailing
    template_name = 'admin_ui.html'
    context_object_name = 'all_mailings'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(MainPage, self).get_context_data(**kwargs)
        return context
    # def get_queryset(self):
    #     queryset = Mailing.objects.all()


class MailingDetailView(DetailView):
    """Детальное представление новости"""
    model = Mailing
    context_object_name = 'mailing'
    template_name = 'mailing_detail.html'

    def get_context_data(self, **kwargs):
        context = super(MailingDetailView, self).get_context_data(**kwargs)
        mailing = self.get_object()
        sent_messages = Message.objects.select_related('client').filter(mailing_id=mailing.id, status='SENT')
        failed_messages = Message.objects.select_related('client').filter(mailing_id=mailing.id, status='FAILED')
        context['sent_messages'] = sent_messages
        context['failed_messages'] = failed_messages

        return context


class MailingCreateView(CreateView):
    """Представление создания новости"""
    model = Mailing
    form_class = MailingForm
    template_name = 'create.html'

    def post(self, request, *args, **kwargs):
        form = MailingForm(request.POST)

        if form.is_valid():
            mailing_instance = form.save(commit=False)
            mailing_instance.save()

            start_time = datetime.strptime(request.POST.get('start_datetime'), '%d/%m/%Y')
            end_time = datetime.strptime(request.POST.get('end_datetime'), '%d/%m/%Y')
            tag = request.POST.get('tag')
            code = request.POST.get('mobile_operator_code')

            if start_time <= datetime.now() < end_time:
                clients = Client.objects.filter(tag=tag, mobile_operator_code=code)
                for client in clients:
                    message = Message.objects.create(
                                mailing=mailing_instance,
                                client=client,
                                status='SENT',
                                created_at=datetime.now()
                    )
                    message.save()

            return redirect(f'/{mailing_instance.id}/')
        else:
            return redirect('create')


class MailingEditFormView(UpdateView):
    """Представление редактирования новости"""
    model = Mailing
    form_class = MailingForm
    template_name = 'edit.html'


class MailingDeleteView(DeleteView):
    """Представление удаления новости"""
    model = Mailing
    template_name = 'delete.html'
    success_url = reverse_lazy('main_page')


class Report(View):
    def get(self, request):
        context = {
            'mailings': Mailing.objects.all(),
            'messages': Message.objects.select_related('mailing', 'client').all().order_by('mailing_id', 'status'),
        }
        return render(request, 'reports/main_report.html', context)



