from django.apps import AppConfig


class AppMailingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_mailings'
