from django.urls import path
from app_mailings.views import MainPage, MailingDetailView, MailingCreateView, MailingEditFormView, MailingDeleteView, Report


urlpatterns = [
    path('', MainPage.as_view(), name='main_page'),
    path('<int:pk>/', MailingDetailView.as_view(), name='mailing_detail'),
    path('create/', MailingCreateView.as_view(), name='mailing_create'),
    path('<int:pk>/edit/', MailingEditFormView.as_view(), name='mailing_edit'),
    path('<int:pk>/delete/', MailingDeleteView.as_view(), name='mailing_delete'),

    path('report/', Report.as_view(), name='main_report'),
]
