from django import forms
from app_mailings_api.models import Mailing, Client, Message
from django.forms import Textarea


class MailingForm(forms.ModelForm):
    start_datetime = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'placeholder': 'day/month/year'}))
    end_datetime = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'placeholder': 'day/month/year'}))


    class Meta:
        model = Mailing
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['text'].widget = Textarea(attrs={'height': '50px',
                                                     'width': '300px',
                                                     'resize': 'none'})
