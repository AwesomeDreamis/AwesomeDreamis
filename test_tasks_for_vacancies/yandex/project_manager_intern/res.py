assessors = {}
with open('Тестовое задание менеджер проекта стажер.csv', 'r', encoding='utf-8') as file:
    file.seek(25)
    for string in file:
        str_list = string.split()
        if str_list[1] not in assessors.keys():
            assessors[str_list[1]] = [0, 1]
            if str_list[3] != str_list[4]:
                assessors[str_list[1]][0] += 1
        else:
            assessors[str_list[1]][1] += 1
            if str_list[3] != str_list[4]:
                assessors[str_list[1]][0] += 1


effectiveness_of_assessors = {}
for uid in assessors.keys():
    effectiveness_of_assessors[uid] = round((assessors[uid][1] - assessors[uid][0]) * 100 / assessors[uid][1], 2)


sorted_assessors = {}
sorted_keys = sorted(effectiveness_of_assessors, key=effectiveness_of_assessors.get)

for i in sorted_keys:
    sorted_assessors[i] = effectiveness_of_assessors[i]

for num, i in enumerate(sorted_assessors):
    print(f'User ID: {i} - Верных ответов: {sorted_assessors[i]}%')
    if num == 9:
        break
