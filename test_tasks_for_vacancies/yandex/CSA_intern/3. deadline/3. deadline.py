try_num = 90

first_prob = 1
second_prob = 1

for i in range(1, try_num + 1):
    first_prob *= 1 - (1 / (i + 1))
    second_prob *= 1 - (1 / (i + 1) ** 2)

print(round(1 - first_prob, 2), round(1 - second_prob, 2))
