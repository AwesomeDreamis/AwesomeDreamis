import random


attempts = 10000

odd_count = 0

for j in range(attempts):
    data = []
    for i in range(1, 101):
        heads_prob = 1 / ((2 * i) + 1)
        result = random.choices([1, 0], weights=[heads_prob, 1 - heads_prob])
        data.append(result[0])

    if sum(data) % 2 == 1:
        odd_count += 1

print(round(odd_count / attempts, 4))


###################################################################
Peven = 1  # четный
Podd = 0  #

for i in range(1, 101):
    head = 1 / (2 * i + 1)  # вероятность выпадения орла
    tail = 1 - head  # вероятность выпадения решки

    Po = Peven * head + Podd * tail  # результат броска?
    Peven = Podd * head + Peven * tail
    Podd = Po

#     Podd = Podd * (tail) + (1 - Podd) * head

print(round(Podd, 4))

###################################################################
# Монету бросают 8 (all) раз. Найти вероятность того, что герб выпадет ровно 4 (success) раза
# from math import factorial
#
# success = 4
# all = 8
#
# n = 2 ** 8
# m = factorial(all) / factorial(success) ** 2
#
# p = m / n
# print(p)


###################################################################
res = 0

for i in range(1, 101):
    head = 1/(2*i+1)
    tail = 1 - head
    res = head * 100

print(round(res, 4))


###################################################################
res = 0

for i in range(1, 101):
    head = 1/(2*i+1)
    tail = 1 - head
    res = head * 100

print(round(res, 4))
