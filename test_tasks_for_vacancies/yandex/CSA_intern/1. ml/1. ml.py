n = 100

# иван
robots1 = (n / 2) * 0.8
r_people1 = (n / 2) * 0.4

precision1 = robots1 / (robots1 + r_people1)
recall1 = robots1 / (robots1 + ((n / 2) - robots1))

f1 = 2 * (precision1 * recall1) / (precision1 + recall1)

# марья
robots2 = (n / 2) * 0.6
r_people2 = (n / 2) * 0.2

precision2 = robots2 / (robots2 + r_people2)
recall2 = robots2 / (robots2 + ((n / 2) - robots2))

f2 = 2 * (precision2 * recall2) / (precision2 + recall2)


abs_precision = precision1 + precision2
abs_recall = recall1 + recall2

abs_f = 2 * (abs_precision * abs_recall) / (abs_precision + abs_recall)


print(round(f1, 6))
print(round(f2, 6))

print((f1 + f2) / 2)
print(round(abs_f, 6))
