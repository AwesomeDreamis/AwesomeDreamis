from datetime import datetime, timedelta

users = {}
date_list = []
sessions = 0

days = {}

time_list = []


with open('log.csv', 'r', encoding='utf-8') as file:
    file.seek(31)
    for string in file:
        str_list = string.split(',')
        if str_list[0].startswith('2020-04-18') and str_list[1] not in users.keys():
            users[str_list[1]] = [str_list[0][11:]]
        elif str_list[0].startswith('2020-04-18') and str_list[1] in users.keys():
            users[str_list[1]] += [str_list[0][11:]]


        date = str_list[0][:10]
        if date not in days.keys() and str_list[2] == '4' and str_list[3].startswith('text'):
            days[date] = [str_list[1]]
        if date in days.keys() and str_list[2] == '4' and str_list[3].startswith('text'):
            if str_list[1] not in days[date]:
                days[date] += [str_list[1]]


        time_list.append(str_list[0])

###################################################################

for i in users:
    if len(users[i]) > 1:
        for time1, time2 in zip(users[i], users[i][1:]):
            time_1 = datetime.strptime(time1, "%H:%M:%S")
            time_2 = datetime.strptime(time2, "%H:%M:%S")
            time_interval = time_2 - time_1

            if time_interval.seconds >= 1800:
                sessions += 1
    else:
        sessions += 1

task_1 = sessions

###################################################################

uniq_users = {}

for i in days:
    uniq_users[i] = len(days[i])

task_2 = max(uniq_users.values())

####################################################################

actions_in_20_min = {}

for i in time_list:
    start_time = datetime.strptime(i, "%Y-%m-%d_%H:%M:%S")
    end_time = start_time + timedelta(minutes=20)
    actions_in_20_min[start_time] = 1

    for j in time_list:
        current_time = datetime.strptime(j, "%Y-%m-%d_%H:%M:%S")

        if start_time <= current_time <= end_time:
            actions_in_20_min[start_time] += 1
        else:
            break

our_time = max(actions_in_20_min, key=actions_in_20_min.get)
task_3 = str(our_time).split()
task_3 = task_3[0] + '_' + task_3[1]

print(task_1, task_2, task_3)
