accessors = {}

with open('answers.csv', 'r', encoding='utf-8') as file:
    file.seek(27)
    for string in file:
        str_list = string[:-1].split(',')
        if str_list[1] not in accessors.keys():
            accessors[str_list[1]] = {str(str_list[2]): 1}
        else:
            if str_list[2] not in accessors[str_list[1]].keys():
                accessors[str_list[1]][str_list[2]] = 1
            else:
                accessors[str_list[1]][str_list[2]] += 1


for i in accessors:
    sorted_tuple = sorted(accessors[i].items(), key=lambda x: x[1], reverse=True)
    accessors[i] = dict(sorted_tuple)

right_answers = {}

for key in accessors:
    for value in accessors[key]:
        right_answers[key] = value
        break

sorted_tuple = sorted(right_answers.items(), key=lambda x: x[0])
right_answers = dict(sorted_tuple)

for i in right_answers:
    print(i, right_answers[i])
