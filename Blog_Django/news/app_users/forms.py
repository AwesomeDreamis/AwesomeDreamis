from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class AuthForm(forms.Form):
    """Форма для логина"""

    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class RegisterForm(UserCreationForm):
    """Форма регистрации"""

    first_name = forms.CharField(max_length=30, required=False, help_text='Имя')
    last_name = forms.CharField(max_length=30, required=False, help_text='Фамилия')
    city = forms.CharField(max_length=36, required=False, help_text='Город')
    phone = forms.CharField(max_length=13, required=False, help_text='Телефон')
    profile_img = forms.ImageField(required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2', 'city', 'phone')


class ProfileEditForm(forms.Form):
    """Форма для изменения профиля"""

    first_name = forms.CharField(max_length=30, required=False, help_text='Имя')
    last_name = forms.CharField(max_length=30, required=False, help_text='Фамилия')
    city = forms.CharField(max_length=36, required=False, help_text='Город')
    phone = forms.CharField(max_length=13, required=False, help_text='Телефон')
    profile_img = forms.ImageField(required=False)
    profile_head_img = forms.ImageField(required=False)

