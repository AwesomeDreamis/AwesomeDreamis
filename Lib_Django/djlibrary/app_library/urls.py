from django.urls import path
from app_library.views import MainPage, AuthorsList, AuthorDetail, BooksList, BookDetail

urlpatterns = [
    path('', MainPage.as_view(), name='main_page'),
    path('api/authors/', AuthorsList.as_view(), name='authors_list'),
    path('api/authors/<int:pk>', AuthorDetail.as_view(), name='author_detail'),
    path('api/books/', BooksList.as_view(), name='books_list'),
    path('api/books/<int:pk>', BookDetail.as_view(), name='book_detail'),

]
