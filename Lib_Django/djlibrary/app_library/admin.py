from django.contrib import admin
from app_library.models import Author, Book


class BooksInline(admin.TabularInline):
    model = Book


@admin.register(Author)
class AuthorsAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', ]
    list_filter = ['last_name', ]
    search_fields = ['first_name', 'last_name', ]
    inlines = [BooksInline]


@admin.register(Book)
class BooksAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'author', 'isbn', 'year', ]
    list_filter = ['title', 'author', ]
    search_fields = ['title', 'author', 'isbn', ]
