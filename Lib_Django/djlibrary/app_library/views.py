from django.shortcuts import render
from django.views import View
from django.http import HttpResponse, JsonResponse
from django.views.generic import ListView
from app_library.models import Author, Book
from app_library.serializers import AuthorSerializer, BookSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.generics import GenericAPIView
from app_library.features import CustomPagination, BooksFilter
from django.db.models import Q


class MainPage(ListView):
    """Главная страница"""

    model = Book
    template_name = 'library/lib.html'
    context_object_name = 'all_books'


class AuthorsList(ListModelMixin, CreateModelMixin, GenericAPIView):
    """API представление для получения списка авторов"""

    serializer_class = AuthorSerializer
    pagination_class = CustomPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['first_name', 'last_name']

    def get_queryset(self) -> dict:
        """
        Получение списка обектов (Авторов)
        :return: Список авторов
        :rtype: dict
        """

        queryset = Author.objects.all()
        search_request = self.request.query_params.get('name')
        if search_request:  # Поиск по имени и фамилии через параметр 'name'
            queryset = queryset.filter(Q(first_name__icontains=search_request) | Q(last_name__icontains=search_request))
        return queryset

    def get(self, request):
        """Получение данных с сервера"""
        return self.list(request)

    def post(self, request, format=None):
        """Передача данных на сервер. Создание объекта"""
        return self.create(request)


class AuthorDetail(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, GenericAPIView):
    """API представление для получения детальной информации об авторе,
    а также для его редактирвоания и удаления"""

    queryset = Author.objects.all()
    serializer_class = AuthorSerializer

    def get(self, request, *args, **kwargs):
        """Получение объекта"""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Изменение объекта"""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Удаление объекта"""
        return self.destroy(request, *args, **kwargs)


class BooksList(ListModelMixin, CreateModelMixin, GenericAPIView):
    """API представление для получения списка книг"""

    queryset = Book.objects.all()
    serializer_class = BookSerializer
    pagination_class = CustomPagination
    filter_backends = [DjangoFilterBackend]
    filterset_class = BooksFilter

    def get_queryset(self) -> dict:
        """
        Получение списка обектов (Книг)
        :return: Список книг
        :rtype: dict
        """

        queryset = Book.objects.all()
        search_author = self.request.query_params.get('author')
        search_title = self.request.query_params.get('title')
        search_year = self.request.query_params.get('year')
        if search_author:  # Поиск по имени и фамилии автора через параметр 'author'
            queryset = queryset.filter(Q(author__first_name__icontains=search_author) | Q(author__last_name__icontains=search_author))
        if search_title:  # Поиск по названию книги 'title'
            queryset = queryset.filter(Q(title__icontains=search_title))
        if search_year:  # Поиск по году публикации через параметр 'year'
            queryset = queryset.filter(Q(year__icontains=search_year))
        return queryset

    def get(self, request):
        """Получение данных с сервера"""
        return self.list(request)

    def post(self, request, format=None):
        """Передача данных на сервер. Создание объекта"""
        return self.create(request)


class BookDetail(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, GenericAPIView):
    """API представление для получения детальной информации об авторе,
    а также для его редактирвоания и удаления"""

    queryset = Book.objects.all()
    serializer_class = BookSerializer

    def get(self, request, *args, **kwargs):
        """Получение объекта"""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Изменение объекта"""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Удаление объекта"""
        return self.destroy(request, *args, **kwargs)
