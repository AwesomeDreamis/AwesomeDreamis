from django.db import models
from django.db.models import CharField


class Author(models.Model):
    """Модель автора."""

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    date_of_birth = models.DateField()

    def __str__(self) -> str:
        """
        Возвращает имя и фамилию автора
        :return: Имя и фамилия
        :rtype: str
        """
        return f'{self.first_name} {self.last_name}'


class Book(models.Model):
    """Модель книги."""

    author = models.ForeignKey(Author, on_delete=models.CASCADE, default=None)
    title = models.CharField(max_length=50)
    isbn = models.CharField(max_length=17)
    year = models.CharField(max_length=4)
    pages = models.IntegerField()

    def __str__(self) -> str:
        """
        Возвращает название книги
        :return: Название
        :rtype: str
        """
        return f'{self.title}'
