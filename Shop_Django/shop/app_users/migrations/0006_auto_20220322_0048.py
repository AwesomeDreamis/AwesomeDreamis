# Generated by Django 2.2 on 2022-03-21 21:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_users', '0005_auto_20220322_0044'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='balance',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='status',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='status_point',
        ),
    ]
