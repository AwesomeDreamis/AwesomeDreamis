from django.urls import path
from app_users.views import Login, Logout, RegisterView, ProfileView, ProfileUpdateView


urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('<int:profile_id>/', ProfileView.as_view(), name='profile'),
    path('<int:profile_id>/edit/', ProfileUpdateView.as_view(), name='profile_edit'),
]
