import os
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from .forms import RegisterForm, ProfileEditForm
from app_users.models import Profile
from app_account.models import Account
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect
from django.views import View
import logging


logger = logging.getLogger(__name__)


class Login(LoginView):
    template_name = 'users/login.html'

    def form_valid(self, form):
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(self.request, user)
        logger.info(f"Пользователь {form.cleaned_data.get('username')} вошёл в систему")
        return HttpResponseRedirect(self.get_success_url())


class Logout(LogoutView):
    next_page = '/'


class RegisterView(View):

    def get(self, request):
        form = RegisterForm()
        return render(request, 'users/register.html', {'form': form})

    def post(self, request):
        form = RegisterForm(request.POST)

        if form.is_valid():
            user = form.save()
            city = form.cleaned_data.get('city')
            phone = form.cleaned_data.get('phone')
            Profile.objects.create(
                user=user,
                city=city,
                phone=phone,
            )

            account = Account.objects.create(
                user=user
            )

            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            logger.info(f'Пользователь {username} вошёл в систему')
            return HttpResponseRedirect('/')


class ProfileView(View):

    def get(self, request, profile_id):
        if not self.request.user.id == profile_id:
            raise PermissionDenied

        user_data = User.objects.get(id=profile_id)
        profile_data = user_data.profile
        account_data = user_data.account
        user_form = RegisterForm(instance=user_data)
        profile_form = RegisterForm(instance=profile_data)

        if not request.user == user_data:
            raise PermissionDenied()

        return render(request, 'users/profile.html', context={'user_form': user_form,
                                                              'profile_form': profile_form,
                                                              'user': user_data,
                                                              'profile': profile_data,
                                                              'account': account_data,
                                                              'profile_id': profile_id,
                                                              })


class ProfileUpdateView(View):

    def get(self, request, profile_id):
        if not self.request.user.id == profile_id:
            raise PermissionDenied

        user = request.user
        user_profile = get_object_or_404(Profile, user=user)
        user_account = get_object_or_404(Account, user=user)
        default_data = {'first_name': user.first_name,
                        'last_name': user.last_name,
                        'city': user_profile.city,
                        'phone': user_profile.phone,
                        'profile_img': user_profile.profile_img,
                        'balance': user_account.balance,
                        }
        form = ProfileEditForm(default_data)

        return render(request, 'users/profile_edit.html', {'form': form, 'user': user})

    def post(self, request, profile_id):
        if not self.request.user.id == profile_id:
            raise PermissionDenied

        user = request.user
        user_profile = get_object_or_404(Profile, user=user)
        user_account = get_object_or_404(Account, user=user)
        default_data = {'first_name': user.first_name,
                        'last_name': user.last_name,
                        'city': user_profile.city,
                        'phone': user_profile.phone,
                        'profile_img': user_profile.profile_img,
                        'balance': user_account.balance,
                        }
        form = ProfileEditForm(request.POST, request.FILES)

        if form.is_valid():
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save(update_fields=['first_name', 'last_name'])

            user_profile.city = form.cleaned_data['city']
            user_profile.phone = form.cleaned_data['phone']
            user_account.balance = form.cleaned_data['balance']

            if form.cleaned_data['profile_img']:
                user_profile.profile_img = form.cleaned_data['profile_img']

            user_profile.save()
            user_account.save()

            if user_account.balance != default_data['balance']:
                summ = user_account.balance - default_data['balance']
                logger.info(f'Пользователь {request.user.username} пополнил баланс на {summ}')

            return HttpResponseRedirect('/users/' + str(profile_id))

        return render(request, 'users/profile_edit.html', {'form': form, 'user': user})
