from django.db import models
from django.contrib.auth.models import User
from app_products.models import Prods
from django.utils.translation import gettext_lazy as _


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_('user'))
    profile_img = models.ImageField(upload_to='profile_images/', null=True, blank=True, default='profile_images/defaultuser.png', verbose_name=_('image'))
    city = models.CharField(max_length=36, blank=True, null=True, default=None, verbose_name=_('city'))
    phone = models.CharField(max_length=13, blank=True, null=True, default=None, verbose_name=_('phone'))
    is_verified = models.BooleanField(null=True, default=False, verbose_name=_('verified'))

    class Meta:
        verbose_name_plural = _('profiles')
        verbose_name = _('profile')


class PurchaseHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_('user'))
    product = models.ForeignKey(Prods, default=None, on_delete=models.CASCADE, verbose_name=_('product'))
    purchase_date = models.DateTimeField(auto_now_add=True, verbose_name=_('purchase date'))

    class Meta:
        verbose_name = _('purchase history')
