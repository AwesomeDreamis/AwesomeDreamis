from django.contrib import admin
from app_users.models import Profile, PurchaseHistory
from django.contrib.auth.models import User


class PurchaseHistoryInLine(admin.TabularInline):
    model = PurchaseHistory


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'id', 'city', 'phone', 'is_verified', 'profile_img']
    list_filter = ['is_verified', 'city']
    actions = ['mark_as_verified', 'mark_as_non_verified']
    search_fields = ['city']

    def mark_as_verified(self, request, queryset):
        queryset.update(is_verified=True)

    def mark_as_non_verified(self, request, queryset):
        queryset.update(is_verified=False)

    mark_as_verified.short_description = 'верифицировать'
    mark_as_non_verified.short_description = 'перевести в статус неверифицирован'


@admin.register(PurchaseHistory)
class PurchaseHistory(admin.ModelAdmin):
    list_display = ['user', 'product', 'purchase_date', ]
    list_filter = ['user', 'purchase_date', ]
