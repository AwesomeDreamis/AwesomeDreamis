import csv
import random


stores = ['Tech', 'Wear', 'Other']

with open('data.csv', mode="w", encoding='utf-8') as file:
    writer = csv.writer(file, delimiter=";", lineterminator="\r")
    for row in range(1000):
        writer.writerow([f"prod{row}", random.choice(stores), "asd", random.randint(1, 500), 10])
