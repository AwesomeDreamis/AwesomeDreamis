from django.contrib import admin
from django.urls import path, include
from app_purchases.views import WishList, SaveProd, DownloadReport, add_prods, ShoppingCart, ShopProd, RemoveProd, PurchaseHistoryView, BuyProds, ProdReport

urlpatterns = [
    path('wishlist/<int:pk>/', WishList.as_view(), name='wishlist'),
    path('shopping_cart/<int:pk>/', ShoppingCart.as_view(), name='shopping_cart'),
    path('purchase_history/<int:pk>/', PurchaseHistoryView.as_view(), name='purchase_history'),

    path('save/<int:pk>', SaveProd.as_view(), name='save_saved'),
    path('shop/<int:pk>', ShopProd.as_view(), name='shop_saved'),

    path('remove/<int:pk>', RemoveProd.as_view(), name='remove_shoped'),
    path('buy/<int:pk>', BuyProds.as_view(), name='buy'),

    path('report/', ProdReport.as_view(), name='report'),
    path('upload/', add_prods, name='upload'),
    path('download_report/', DownloadReport.as_view(), name='download_report'),
]
