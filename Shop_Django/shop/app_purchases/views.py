import os
from _csv import reader
from django.db.models import Sum, Count
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from app_products.models import Prods, Store
from app_users.models import PurchaseHistory
from app_purchases.forms import UploadPostForm
from django.views.generic import ListView
import logging


logger = logging.getLogger(__name__)


class WishList(View):
    def get(self, request, pk):
        if not self.request.user.id == pk:
            raise PermissionDenied

        user = User.objects.get(id=pk)
        saved_prods = Prods.objects.filter(saves=user)

        return render(request, 'purchases/wishlist.html', context={'user': user,
                                                                   'saved_prods': saved_prods
                                                                   })


class SaveProd(View):
    def post(self, request, pk):
        if not self.request.user.id == pk:
            raise PermissionDenied

        user = request.user
        prod_id = request.POST.get('prod_id')
        prod_obj = Prods.objects.only('saves').get(id=prod_id)

        if user in prod_obj.saves.all():
            prod_obj.saves.remove(user)
        else:
            prod_obj.saves.add(user)

        return HttpResponseRedirect(reverse('wishlist', args=[str(pk)]))


class ShopProd(View):
    def post(self, request, pk):
        user = request.user
        prod_id = request.POST.get('prod_id')
        prod_obj = Prods.objects.only('buyers').get(id=prod_id)

        if user in prod_obj.buyers.all():
            prod_obj.buyers.remove(user)
        else:
            prod_obj.buyers.add(user)

        return HttpResponseRedirect(reverse('wishlist', args=[str(pk)]))


#############################################################################################

class ShoppingCart(View):
    def get(self, request, pk):
        if not self.request.user.id == pk:
            raise PermissionDenied

        user = User.objects.get(id=pk)
        prods_in_shopping_cart = Prods.objects.filter(buyers=user)

        total_ammount = 0
        for prod in prods_in_shopping_cart:
            if prod.quantity > 0:
                if prod.is_promoted:
                    total_ammount += prod.promo_price
                else:
                    total_ammount += prod.price

        return render(request, 'purchases/shopping_cart.html', context={'user': user,
                                                                        'prods_in_shopping_cart': prods_in_shopping_cart,
                                                                        'total_ammount': total_ammount,
                                                                        })


class RemoveProd(View):
    def post(self, request, pk):
        if not self.request.user.id == pk:
            raise PermissionDenied

        user = request.user
        prod_id = request.POST.get('prod_id')
        prod_obj = Prods.objects.only('buyers').get(id=prod_id)

        if user in prod_obj.buyers.all():
            prod_obj.buyers.remove(user)
        else:
            prod_obj.buyers.add(user)

        return HttpResponseRedirect(reverse('shopping_cart', args=[str(pk)]))


class BuyProds(View):
    def post(self, request, pk):
        if not self.request.user.id == pk:
            raise PermissionDenied

        user = request.user
        prods_in_shopping_cart = Prods.objects.filter(buyers=user)

        total_ammount = 0

        for prod in prods_in_shopping_cart:
            if prod.quantity > 0:
                if prod.is_promoted:
                    total_ammount += prod.promo_price
                else:
                    total_ammount += prod.price
            else:
                if user in prod.saves.all():
                    pass
                else:
                    prod.saves.add(user)

        if user.account.can_purchase_amount(total_ammount):
            with transaction.atomic():
                prods_to_history = []
                for prod in prods_in_shopping_cart:
                    prods_to_history.append(PurchaseHistory(user=user, product=prod))
                    prod.buyers.remove(user)
                    prod.sold_quantity += 1
                    prod.quantity -= 1
                    prod.save(update_fields=['sold_quantity', 'quantity'])

                PurchaseHistory.objects.bulk_create(prods_to_history)
                logger.info(f'Пользователь {request.user.username} приобрёл товары: {[i.product.title for i in prods_to_history]} на сумму {total_ammount}')

                user.account.buy(total_ammount)
                logger.info(f'С баланса {request.user.username} списано {total_ammount} балла')

                user.account.status_point += total_ammount

                if 1000 <= user.account.status_point < 10000:
                    user.account.status = 2
                    logger.info(f'Пользователь {request.user.username} перешёл на уровень "advanced"')
                elif user.account.status_point >= 10000:
                    user.account.status = 3
                    logger.info(f'Пользователь {request.user.username} перешёл на уровень "expert"')
                user.account.save(update_fields=['status', 'status_point', 'balance'])
        else:
            return HttpResponse('Insufficient funds to pay ')

        return HttpResponseRedirect(reverse('shopping_cart', args=[str(pk)]))


#############################################################################################


class PurchaseHistoryView(View):
    def get(self, request, pk):
        if not self.request.user.id == pk:
            raise PermissionDenied

        user = User.objects.get(id=pk)
        purchased_prods = PurchaseHistory.objects.filter(user=user)

        return render(request, 'purchases/purchase_history.html', context={'user': user,
                                                                           'purchased_prods': purchased_prods
                                                                           })


#############################################################################################

class ProdReport(ListView):
    model = PurchaseHistory
    context_object_name = 'prods'
    template_name = 'purchases/report.html'

    def get_queryset(self):
        if not self.request.user.groups.filter(name='moderators').exists():
            raise PermissionDenied

        if self.request.GET.get('start_date') and self.request.GET.get('end_date'):
            date_from = self.request.GET.get('start_date')
            date_to = self.request.GET.get('end_date')

            queryset = PurchaseHistory.objects\
                .select_related('product')\
                .filter(purchase_date__range=[date_from, date_to])\
                .values('product_id', 'product__title')\
                .annotate(Count('product'))\
                .order_by('-product__count')

            # queryset = Prods.objects\
            #     .filter(purchasehistory__purchase_date__range=[date_from, date_to])\
            #     .values('id', 'title')\
            #     .annotate(Count('id'))\
            #     .order_by('-id__count')
        else:
            queryset = PurchaseHistory.objects\
                .select_related('product')\
                .values('product_id', 'product__title')\
                .annotate(Count('product'))\
                .order_by('-product__count')

            # queryset = Prods.objects.filter(purchasehistory__product=Prods.objects.all())\
            #     .values('id', 'title') \
            #     .annotate(Count('id')) \
            #     # .order_by('-id__count')

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ProdReport, self).get_context_data(**kwargs)
        data = list(self.get_queryset())
        new_data = []
        for row in data:
            l = []
            for key in row:
                l.append(row[key])
            new_data.append(l)
        context['data'] = new_data
        return context


class DownloadReport(View):
    def post(self, request):
        import csv
        # file_path = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
        # if os.path.exists(file_path):
        with open('report.csv', 'wb') as file:
            response = HttpResponse(content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="report.csv"'

            report_data = self.request.POST.get('report_data')
            data = report_data[2:len(report_data) - 2:].split('], [')

            writer = csv.writer(response, delimiter=';')
            writer.writerow(['id', 'title', 'quantity'])
            for row in data:
                writer.writerow(row.split(','))

            return response
        # raise Http404


#############################################################################################

def add_prods(request):
    if request.method == 'POST':
        upload_file_form = UploadPostForm(request.POST, request.FILES)
        if upload_file_form.is_valid():
            post_file = upload_file_form.cleaned_data['file'].read()
            post_str = post_file.decode('utf-8-sig').split('\n')
            csv_reader = reader(post_str, delimiter=";")

            prods_to_insert = []

            for row in csv_reader:
                if len(row) == 5:
                    prods_to_insert.append(Prods(
                        title=row[0],
                        store=Store.objects.get(title=row[1]),
                        description=row[2],
                        price=int(row[3]),
                        quantity=int(row[4]),
                        ))

            Prods.objects.bulk_create(prods_to_insert)

            return HttpResponseRedirect('/')
    else:
        upload_file_form = UploadPostForm()

    context = {'form': upload_file_form}
    return render(request, 'purchases/upload_file.html', context=context)

