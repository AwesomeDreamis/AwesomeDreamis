from django.test import TestCase
from django.urls import reverse
from app_products.models import Prods, Store
from app_users.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.test import Client


class WishlistPageTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username="testuser",)
        user.set_password('12345')
        user.save()

        cls.profile = Profile.objects.create(
            user=user,
            city="moscow",
            phone="555",
            is_verified=False,
        )

    def test_wishlist_page(self):
        logged_in = self.client.login(username='testuser', password='12345')
        response = self.client.get(f'/purchases/wishlist/{self.profile.user.id}/')

        self.assertTrue(logged_in)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'purchases/wishlist.html')


class ShoppingCartPageTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username="testuser",)
        user.set_password('12345')
        user.save()

        cls.profile = Profile.objects.create(
            user=user,
            city="moscow",
            phone="555",
            is_verified=False,
        )

    def test_shopping_cart_page(self):
        logged_in = self.client.login(username='testuser', password='12345')
        response = self.client.get(f'/purchases/shopping_cart/{self.profile.user.id}/')

        self.assertTrue(logged_in)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'purchases/shopping_cart.html')


class PurchaseHistoryPageTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username="testuser",)
        user.set_password('12345')
        user.save()

        cls.profile = Profile.objects.create(
            user=user,
            city="moscow",
            phone="555",
            is_verified=False,
        )

    def test_purchase_history_page(self):
        logged_in = self.client.login(username='testuser', password='12345')
        response = self.client.get(f'/purchases/purchase_history/{self.profile.user.id}/')

        self.assertTrue(logged_in)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'purchases/purchase_history.html')


# class ProdListPageTest(TestCase):
#     def test_create_page(self):
#         url = reverse('prod_list')
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)
#
#     def test_create_exists_at_desired_location(self):
#         response = self.client.get('/')
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'products/prod_list.html')
# #
#
# class NewsDetailPageTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         user = User.objects.create(username="test",)
#         user.set_password('12345')
#         user.save()
#
#         cls.news = News.objects.create(
#             author=user,
#             content="test",
#             is_active=False
#         )
#
#     def test_news_list_exists_at_desired_location(self):
#         response = self.client.get(f'/{self.news.id}/')
#         self.assertEqual(response.status_code, 200)
#         # self.assertTemplateUsed(response, 'news/news_detail.html')
#
#     def test_comment_post(self):
#         response = self.client.post(f'/{self.news.id}/', {'user': self.news.author, 'text': 'text'})
#         self.assertEqual(response.status_code, 200)
#
#
# class NewsEditPageTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         user = User.objects.create(username="test",)
#         user.set_password('12345')
#         user.save()
#
#         cls.news = News.objects.create(
#             author=user,
#             content="test",
#             is_active=False
#         )
#
#     def test_news_edit_exists_at_desired_location(self):
#         logged_in = self.client.login(username='test', password='12345')
#         response = self.client.get(f'/{self.news.id}/edit/')
#
#         self.assertTrue(logged_in)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'news/edit.html')
#
#
# class NewsDeletePageTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         user = User.objects.create(username="test",)
#         user.set_password('12345')
#         user.save()
#
#         cls.news = News.objects.create(
#             author=user,
#             content="test",
#             is_active=False
#         )
#
#     def test_news_delete_exists_at_desired_location(self):
#         logged_in = self.client.login(username='test', password='12345')
#         response = self.client.get(f'/{self.news.id}/delete/')
#
#         self.assertTrue(logged_in)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'news/delete.html')
