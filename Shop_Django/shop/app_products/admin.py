from django.contrib import admin
from app_products.models import Prods, Images, Store


class ProdsImagesInline(admin.TabularInline):
    model = Images


class ProdsInline(admin.TabularInline):
    model = Prods


@admin.register(Images)
class ImagesAdmin(admin.ModelAdmin):
    list_display = ['id', 'prod']


@admin.register(Prods)
class ProdsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'price', 'store', 'quantity', 'sold_quantity']
    list_filter = ['store']
    search_fields = ['title', 'description', 'shop']
    inlines = [ProdsImagesInline]
    actions = ['increase_quantity']

    def increase_quantity(self, request, queryset):
        queryset.update(quantity=10)

    increase_quantity.short_description = 'увеличить запас на складе'


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'description', ]
    list_filter = ['title']
    search_fields = ['title', 'description', ]

    inlines = [ProdsInline]

