from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class Store(models.Model):
    title = models.CharField(max_length=100, db_index=True, default=None, null=True, verbose_name=_('title'))
    description = models.CharField(max_length=1500, default='', verbose_name=_('description'))
    image = models.ImageField(upload_to='store_images/', null=True, blank=True, default='store_images/default.png', verbose_name=_('image'))
    is_promoted = models.BooleanField(default=False, verbose_name=_('is promoted'))
    discount = models.PositiveIntegerField(default=0, verbose_name=_('discount'))

    class Meta:
        verbose_name_plural = _('Stores')
        verbose_name = _('Store')

    def __str__(self):
        return self.title


class Prods(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('store'), default=None)
    title = models.CharField(max_length=1500, db_index=True, default=None, null=True, verbose_name=_('title'))
    description = models.CharField(max_length=1500, default='', verbose_name=_('description'))
    price = models.PositiveIntegerField(default=0, verbose_name=_('price'))
    saves = models.ManyToManyField(User, related_name='saves', verbose_name=_('saves'))
    buyers = models.ManyToManyField(User, related_name='buyers', verbose_name=_('buyers'))
    quantity = models.PositiveIntegerField(default=0, verbose_name=_('quantity'))
    sold_quantity = models.PositiveIntegerField(default=0, verbose_name=_('sold quantity'))

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('created_at'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('updated_at'))
    is_active = models.BooleanField(default=False, verbose_name=_('is_active'))
    is_promoted = models.BooleanField(default=False, verbose_name=_('is promoted'))
    promo_price = models.PositiveIntegerField(default=0, verbose_name=_('promo price'))
    discount = models.PositiveIntegerField(default=0, verbose_name=_('discount'))

    class Meta:
        verbose_name_plural = _('prods')
        verbose_name = _('prod')

    def __str__(self):
        return self.title


class Images(models.Model):
    prod = models.ForeignKey(Prods, default=None, on_delete=models.CASCADE, verbose_name=_('prods'))
    image = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name=_('image'))

    class Meta:
        verbose_name_plural = _('images')
        verbose_name = _('image')
