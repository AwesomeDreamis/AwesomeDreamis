from django.test import TestCase
from django.urls import reverse
from app_products.models import Prods, Store
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.test import Client


class StoresListPageTest(TestCase):
    def test_news_list_page(self):
        url = reverse('store_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_news_list_exists_at_desired_location(self):
        response = self.client.get('/stores/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'products/store_list.html')


class ProdListPageTest(TestCase):
    def test_create_page(self):
        url = reverse('prod_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_exists_at_desired_location(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'products/prod_list.html')
