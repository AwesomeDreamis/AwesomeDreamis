from django import forms
from app_news_board.models import Store, Prods, Images
from django.forms import Textarea


class ProdForm(forms.ModelForm):
    saves = forms.IntegerField(required=False)
    buyers = forms.IntegerField(required=False)

    class Meta:
        model = Prods
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
        self.fields['description'].widget = Textarea(attrs={'rows': 10, 'cols': 50})


class ProdImagesForm(forms.ModelForm):
    images = forms.ImageField(required=False, widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = Images
