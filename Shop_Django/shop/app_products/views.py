import os
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View, generic
from django.http import HttpResponse, JsonResponse
from app_products.models import Prods, Images, Store
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DeleteView, DetailView
from django.db.models import F, Q, Prefetch
from datetime import datetime
from django.template.loader import render_to_string
import logging


logger = logging.getLogger(__name__)


class StoresListView(ListView):
    model = Store
    template_name = 'products/store_list.html'
    context_object_name = 'all_stores'

    def get_queryset(self):
        if self.request.GET.get('q'):
            query = self.request.GET.get('q')
            queryset = Store.objects.filter(Q(title__icontains=query))
            return queryset
        else:
            queryset = Store.objects.all()
            return queryset


class ProdListView(ListView):
    model = Prods
    template_name = 'products/prod_list.html'
    context_object_name = 'all_prods'

    def get_queryset(self):
        if self.request.GET.get('q'):
            query = self.request.GET.get('q')
            queryset = Prods.objects.select_related('store').filter(Q(title__icontains=query)).order_by('-created_at')
            return queryset
        else:
            queryset = Prods.objects.select_related('store').order_by('-created_at')
            return queryset

    def get_context_data(self, **kwargs):
        context = super(ProdListView, self).get_context_data(**kwargs)
        return context


class ProdDetailView(DetailView):
    pass


#########################################################################################


class SaveProd(View):
    def post(self, request):
        user = request.user
        post_id = request.POST.get('post_id')
        post_obj = Prods.objects.get(id=post_id)

        if user in post_obj.saves.all():
            post_obj.saves.remove(user)
        else:
            post_obj.saves.add(user)

        return redirect('prod_list')


class AddToShoppingCartProd(View):
    def post(self, request):
        user = request.user
        post_id = request.POST.get('post_id')
        post_obj = Prods.objects.get(id=post_id)

        if user in post_obj.buyers.all():
            post_obj.buyers.remove(user)
        else:
            post_obj.buyers.add(user)

        return redirect('prod_list')
