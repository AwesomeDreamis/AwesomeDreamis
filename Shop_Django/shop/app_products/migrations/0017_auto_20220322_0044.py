# Generated by Django 2.2 on 2022-03-21 21:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_products', '0016_auto_20220307_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='prods',
            name='quantity',
            field=models.PositiveIntegerField(default=0, verbose_name='quantity'),
        ),
        migrations.AddField(
            model_name='prods',
            name='sold_quantity',
            field=models.PositiveIntegerField(default=0, verbose_name='sold quantity'),
        ),
    ]
