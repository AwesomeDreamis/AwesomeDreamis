# Generated by Django 2.2 on 2022-03-06 21:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_products', '0012_store_image'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='store',
            options={'verbose_name': 'Store', 'verbose_name_plural': 'Stores'},
        ),
    ]
