from django.contrib import admin
from django.urls import path, include
from app_products.views import StoresListView, ProdListView, ProdDetailView, SaveProd, AddToShoppingCartProd


urlpatterns = [
    path('', ProdListView.as_view(), name='prod_list'),
    path('stores/', StoresListView.as_view(), name='store_list'),
    # path('', ProdDetailView.as_view(), name='prod_detail'),
    path('save/', SaveProd.as_view(), name='save_list'),
    path('add_to_shopping_cart/', AddToShoppingCartProd.as_view(), name='add_to_shopping_cart'),
]
