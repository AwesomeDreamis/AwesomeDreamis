from _csv import reader

from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from app_products.models import Prods, Store
from app_account.models import Account
from django.core.cache import cache
from django.views.generic import DetailView


def set_promo_stores(stores):
    promo_stores = {}
    for store_title in stores:
            store = Store.objects.get(title=store_title)
            store.is_promoted = True
            store.discount = stores[store_title]
            store.save(update_fields=['is_promoted', 'discount'])

            promo_stores[store.title] = store.discount

            for i in Prods.objects.filter(store__title=store_title):
                if i.store.id == store.id:

                    promo_product = Prods.objects.get(id=i.id)
                    promo_product.is_promoted = True
                    promo_product.discount = store.discount
                    promo_product.promo_price = round((promo_product.price - (promo_product.price * store.discount / 100)), 2)
                    promo_product.save(update_fields=['is_promoted', 'discount', 'promo_price'])
    return promo_stores


def set_promo_products(prods):
    promo_prods = {}
    for prod in Prods.objects.filter(id__in=prods):
        prod.is_promoted = True
        if prod.discount:
            prod.discount += prods[str(prod.id)]
            prod.promo_price = round((prod.price - (prod.price * prod.discount / 100)), 2)
        prod.save(update_fields=['is_promoted', 'discount', 'promo_price'])

        promo_prods[prod.title] = prods[str(prod.id)]
    return promo_prods


class AccountView(DetailView, UserPassesTestMixin):
    model = Account
    template_name = 'promo/account.html'

    def test_func(self):
        account = self.get_object()
        return account.user == self.request.user

    def get(self, request, *args, **kwargs):
        user = User.objects.get(id=self.request.user.id)

        promo_stores_cache_key = f'promo_stores:{user.username}'
        promo_products_cache_key = f'promo_products:{user.username}'

        stores = {'Tech': 10,
                  'Wear': 30}
        promo_stores = set_promo_stores(stores)

        prods = {'14': 15}
        promo_prods = set_promo_products(prods)

        user_account_cache_data = {
            promo_stores_cache_key: promo_stores,
            promo_products_cache_key: promo_prods
        }
        cache.set_many(user_account_cache_data)

        return render(request, 'promo/account.html', context={'user': user,
                                                              'promo_stores': promo_stores,
                                                              'promo_products': promo_prods,
                                                              })
