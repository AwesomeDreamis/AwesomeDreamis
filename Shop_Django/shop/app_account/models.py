from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_('user'))
    balance = models.PositiveIntegerField(default=0, verbose_name=_('balance'))

    STATUS_LEVELS = [(1, 'Beginner'), (2, 'Advanced'), (3, 'Expert'), ]
    status = models.PositiveIntegerField(choices=STATUS_LEVELS, default=1, verbose_name=_('status'))
    status_point = models.PositiveIntegerField(default=0, verbose_name=_('status point'))

    class Meta:
        verbose_name_plural = _('accounts')
        verbose_name = _('account')

    def buy(self, amount):
        self.balance = self.balance - amount

    def can_purchase_amount(self, amount):
        if amount <= self.balance:
            return True
