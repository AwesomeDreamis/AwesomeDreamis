from django.contrib import admin
from app_account.models import Account
from django.contrib.auth.models import User


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ['user', 'balance']
