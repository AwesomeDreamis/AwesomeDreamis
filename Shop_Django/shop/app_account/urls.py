from django.urls import path, include
from app_account.views import AccountView


urlpatterns = [
    path('<int:pk>/', AccountView.as_view(), name='account'),
]
