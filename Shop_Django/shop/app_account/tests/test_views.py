from django.test import TestCase
from django.urls import reverse
from app_products.models import Prods, Store
from app_users.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.test import Client


class AccountPageTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username="testuser",)
        user.set_password('12345')
        user.save()

        cls.profile = Profile.objects.create(
            user=user,
            city="moscow",
            phone="555",
            is_verified=False,
        )

    def test_account_page(self):
        logged_in = self.client.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

        try:
            response = self.client.get(f'/account/{self.profile.user.id}/')

            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'promo/account.html')
        except Store.DoesNotExist:
            pass

