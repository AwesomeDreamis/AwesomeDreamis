# Жадный алгоритм — алгоритм, заключающийся в принятии локально оптимальных решений на каждом этапе,
# допуская, что конечное решение также окажется оптимальным.

# Пример алгоритма поиска радиостанций

# Сначала составьте список штатов:
states_needed = {"mt", "wa", "or", "id", "nv", "ut", "са", "az"}  # Переданный массив преобразуется в множество

# затем список станций
stations = {}
stations["one"] = {"id", "nv", "ut"}
stations["two"] = {"wa", "id", "mt"}
stations["three"] = {"or", "nv", "са"}
stations["four"] = {"nv", "ut"}
stations["five"] = {"ca", "az"}


final_stations = set()

while states_needed:
    best_station = None
    states_covered = set()

    for station, states_for_station in stations.items():
        covered = states_needed & states_for_station
        if len(covered) > len(states_covered):  # Новый синтаксис! Эта операция называется "пересечением множеств"
            best_station = station
            states_covered = covered

    states_needed -= states_covered
    final_stations.add(best_station)

print(final_stations)


# Множества
#
# > Объединение множеств означает слияние элементов обоих множеств.
# > Под операцией пересечения множеств понимается поиск элементов, входящих в оба множества.
# > Под разностью множеств понимается исключение из одного множества элементов, присутствующих в другом множестве.
#
# Пример:
# fruits = set(["avocado", "tomato", "banana"])
# vegetables = set(["beets", "carrots", "tomato"])
# fruits | vegetables  # Объединение множеств
# set(["avocado", "beets", "carrots", "tomato", "banana"])
# fruits & vegetables  # Пересечение множеств
# set( [ "tomato"])
# fruits - vegetables  # Разность множеств
# set(["avocado", "banana"])
# vegetables - fruits
# set(["beets", "carrots"])
#
# > множества похожи на списки, но множества не содержат дубликатов;
# > с множествами можно выполнять различные интересные операции -
# вычислять их объединение, пересечение и разность.
