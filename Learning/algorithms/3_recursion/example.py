# Псевдокод поиска ключа в куче коробок

# def look_for_key(main_box):
#     pile = main_box.make_a_pile_to_look_through()
#     while pile is not empty:
#         box = pile.grab_a_box()
#         for item in box:
#             if item.is_a_box():
#                 pile.append(item)
#             elif item.is_a_key():
#                 print("found the key!")
# ==
# def look_for_key(box):
#     for item in box:
#         if item.is_a_box():
#             look_for key(item)
#     elif item.is_a_key():
#         print("found the key!")


# "Циклы могут ускорить работу программы. Рекурсия может ускорить работу программиста.
#  Выбирайте, что важнее в вашей ситуации!"


def countdown(i):
    print(i)
    if i <= 0:  # Базовый случай
        return
    else:  # Рекурсивный  случай
        countdown(i - 1)


countdown(10)


########################################################

def greet2(name):
    print("how are you, " + name + "?")
def bуе():
    print("ok bуе!")

def greet(name):
    print("hello, " + name + "!")
    greet2(name)
    print("getting ready to say bуе ... ")
    bуе()

greet('alex')


########################################################

def factorial(x):
    if x == 1:
        return 1
    else:
        return x * factorial(x-1)


print(factorial(5))


# Шпаргалка
# > Когда функция вызывает саму себя, это называется рекурсией.
# > В каждой рекурсивной функции должно быть два случая: базовый и рекурсивный.
# > Стек поддерживает две операции: занесение и извлечение элементов.
# > Все вызовы функций сохраняются в стеке вызовов.
# > Если стек вызовов станет очень большим, он займет слишком много памяти.
