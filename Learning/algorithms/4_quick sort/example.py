# Алгоритм Евклида
a = 1680
b = 640

while a != 0 and b != 0:
    if a > b:
        a = a % b
    else:
        b = b % a

print(a + b)


# быстрая сортировка
def quicksort(array):
    if len(array) < 2:
        return array
    else:
        pivot = array[0]  # опорный элемент
        less = [i for i in array[1:] if i <= pivot]
        greater = [i for i in array[1:] if i > pivot]

        return quicksort(less) + [pivot] + quicksort(greater)


print(quicksort([10, 5, 2, 3]))


# Сортировка слиянием
def merge(left, right, merged):
    left_cursor, right_cursor = 0, 0
    while left_cursor < len(left) and right_cursor < len(right):

        # Сортируем каждый и помещаем в результат
        if left[left_cursor] <= right[right_cursor]:
            merged[left_cursor + right_cursor] = left[left_cursor]
            left_cursor += 1
        else:
            merged[left_cursor + right_cursor] = right[right_cursor]
            right_cursor += 1

    for left_cursor in range(left_cursor, len(left)):
        merged[left_cursor + right_cursor] = left[left_cursor]

    for right_cursor in range(right_cursor, len(right)):
        merged[left_cursor + right_cursor] = right[right_cursor]

    return merged


def merge_sort(arr):
    # Последнее разделение массива
    if len(arr) <= 1:
        return arr
    mid = len(arr) // 2
    # Выполняем merge_sort рекурсивно с двух сторон
    left, right = merge_sort(arr[:mid]), merge_sort(arr[mid:])

    # Объединяем стороны вместе
    return merge(left, right, arr.copy())


print(merge_sort([10, 5, 2, 3]))

# Шпаргалка

# > Стратегия «разделяй и властвуй» основана на разбиении задачи на уменьшающиеся фрагменты.
#   Если вы используете стратегию «разделяй и властвуй» со списком,
#   то базовым случаем, скорее всего, является пустой массив или массив из одного элемента.
# > Если вы реализуете алгоритм быстрой сортировки, выберите в качестве опорного случайный элемент.
#   Среднее время выполнения быстрой сортировки составляет О(n * log n)!
# > Константы в «О-большом» иногда могут иметь значение.
#   Именно по этой причине быстрая сортировка быстрее сортировки слиянием.
# > При сравнении простой сортировки с бинарной константа почти никогда роли не играет,
#   потому что O(log п) слишком сильно превосходит О(n) по скорости при большом размере списка.


# сортировка пузырьком (bubble sort)
def bubble_sort(arr):
    def swap(i, j):
        arr[i], arr[j] = arr[j], arr[i]

    n = len(arr)
    swapped = True
    x = -1

    while swapped:
        swapped = False
        x = x + 1

        for i in range(1, n - x):
            if arr[i - 1] > arr[i]:
                swap(i - 1, i)
                swapped = True


print(bubble_sort([10, 5, 2, 3]))


# сортировка вставками (insertion sort)
def insertion_sort(arr):
    for i in range(len(arr)):
        cursor = arr[i]
        pos = i

        while pos > 0 and arr[pos - 1] > cursor:
            # Меняем местами число, продвигая по списку
            arr[pos] = arr[pos - 1]
            pos = pos - 1
        # Остановимся и сделаем последний обмен
        arr[pos] = cursor

    return arr


print(insertion_sort([10, 5, 2, 3]))

#

#
