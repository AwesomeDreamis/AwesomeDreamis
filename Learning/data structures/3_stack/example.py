stack = []

# append() function to push element in the stack
stack.append("a")
stack.append("b")
stack.append("c")
print("Initial stack")
print(stack)

# pop() function to pop element from stack in LIFO order
print("\nElements popped from stack:")
print(stack.pop())
print(stack.pop())
print(stack.pop())

print("\nStack after elements are popped:")
print(stack)

# uncommenting print(stack.pop()) will cause an IndexError as the stack is now empty


#######################################################################

# Преимущества:
#
# Предлагает управление данными LIFO, которое невозможно с массивами.
# Автоматическое масштабирование и очистка объекта.
# Простая и надёжная система хранения данных.


# Недостатки:
#
# Память стека ограничена.
# Слишком много объектов в стеке приводит к ошибке переполнения стека.


# Приложения:
#
# Используется для создания высокореактивных систем.
# Системы управления памятью используют стеки для обработки в первую очередь самых последних запросов.
# Полезно для таких вопросов, как сопоставление скобок.
