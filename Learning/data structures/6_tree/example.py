class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def insert(self, data):
        # Compare the new value with the parent node
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)

            elif data > self.data:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data

    # Print the tree
    def print_tree(self):
        if self.left:
            self.left.print_tree()

        print(self.data),

        if self.right:
            self.right.print_tree()

# Use the insert method to add nodes
root = Node(12)

root.insert(6)
root.insert(14)
root.insert(3)

root.print_tree()


#######################################################################

# Преимущества:
#
# Подходит для представления иерархических отношений.
# Динамический размер, отличный масштаб.
# Операции быстрой вставки и удаления.
# В двоичном дереве поиска вставленные узлы сразу же упорядочиваются..
# Деревья двоичного поиска эффективны при поиске; длина толькоO (высота)О ( ч е я г ч т ).
# Читайте также:  Как использовать WordPress в качестве Headless CMS для Eleventy


# Недостатки:
#
# Время дорогое, O (войти) 4O ( l o g n ) 4, чтобы изменить или «сбалансировать» деревья или извлечь элементы из известного местоположения.
# Дочерние узлы не содержат информации о своих родительских узлах, и их трудно перемещать назад.
# Работает только для отсортированных списков. Несортированные данные превращаются в линейный поиск.


# Приложения:
#
# Отлично подходит для хранения иерархических данных, таких как расположение файла.
# Используется для реализации лучших алгоритмов поиска и сортировки, таких как двоичные деревья поиска и двоичные кучи.
