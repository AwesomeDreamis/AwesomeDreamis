from collections import deque

# Initializing a queue
q = deque()

# Adding elements to a queue
q.append("a")
q.append("b")
q.append("c")
print("Initial queue")
print(q)

# Removing elements from a queue
print("\nElements dequeued from the queue")
print(q.popleft())
print(q.popleft())

print("\nQueue after removing elements")
print(q)

# Uncommenting q.popleft() will raise an IndexError as queue is now empty


#######################################################################

# Преимущества:
#
# Автоматически упорядочивает данные в хронологическом порядке.
# Весы в соответствии с требованиями к размеру.
# Эффективное время с dequeклассом.


# Недостатки:
#
# Доступ к данным возможен только на концах.


# Приложения:
#
# Операции с общим ресурсом, таким как принтер или ядро ЦП.
# Служит временным хранилищем для пакетных систем.
# Обеспечивает простой порядок по умолчанию для задач равной важности.
