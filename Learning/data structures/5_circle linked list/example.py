class Node:
    def __init__(self, dataval=None):
        self.dataval = dataval
        self.nextval = None


class SLinkedList:
    def __init__(self):
        self.headval = None


list1 = SLinkedList()
list1.headval = Node("Mon")

e2 = Node("Tue")
e3 = Node("Wed")

# Link first Node to second node
list1.headval.nextval = e2

# Link second Node to third node
e2.nextval = list1.headval


#######################################################################

# Преимущества:
#
# Может просматривать весь список, начиная с любого узла.
# Делает связанные списки более подходящими для циклических структур.


# Недостатки:
#
# Сложнее найти узлы Head и Tail списка без nullмаркера.


# Приложения:
#
# Регулярно зацикливающиеся решения, такие как планирование ЦП.
# Решения, в которых вам нужна свобода начать обход с любого узла.
